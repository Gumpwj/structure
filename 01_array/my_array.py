# encoding: utf-8
"""
__author__ = wujian
Array
"""
class MyArray(object):
    """A simple wrapper around list
    You cannot have -1 in the array
    """

    def __init__(self, capacity: int):
        self._data = []
        self._capacity = capacity


    def __getitem__(self, index: int) -> object:
        return self._data[index]

    def __setitem__(self, index: int, value: object):
        self._data[index] = value

    def __len__(self) -> int:
        return len(self._data)
    
    def __iter__(self):
        for item in self._data:
            yield item


    def find(self, index: int) -> object:
        try:
            return self._data[index]
        except IndexError as e:
            print(e)
            return False

    def delete(self, index: int ) -> bool:
        try:
            self._data.pop(index)
            return True
        except IndexError as e:
            print(e)
            return False

    def insert(self, index: int, value: object) -> bool:
        if len(self) > self._capacity:
            return False
        else:
            self._data.insert(index, value)
            return True

    def print_all(self):
        for item in self:
            print(item)


def test_myarray():
    array = MyArray(5)
    array.insert(0, 4)
    array.insert(1, 5)
    array.insert(3, 9)
    array.insert(4, 10)
    # assert array.insert(0, 100) is False
    array.print_all()
    print(array[0])
    assert array[0] == 4

if __name__ == "__main__":
    test_myarray()