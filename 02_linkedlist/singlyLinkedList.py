__author__ = 'wujian'

class Node(object):
    def __int__(self, data, next=None):
        """链表结构节点
        :param data: 存储的数据
        :param next: 指向下一个节点的引用地址
        :return:
        """
        self.__data = data
        self.__next = next

    @property
    def data(self):
        """返回当前节点的值
        :return:
        """
        return self.__data

    @data.setter
    def data(self, data):
        """Node 节点存储数据的方法
        :param data: 新的节点的值
        :return:
        """
        self.__data = data

    @property
    def next(self):
        """返回当前节点存储的下一个节点的引用地址
        :return:
        """
        return self.__next

    @next.setter
    def next(self, next):
        """Node 节点next指针的修改方法
        :param next: 新的下一个Node节点的引用
        :return:
        """
        self.__next = next


class SingleLinkedList(object):
    """ 单向链表 """

    def __int__(self):
        self.__head : Node = None

    def find_by_value(self, value: object) -> Node:
        """按照数值在单向列表中寻找
        :param value: 需要查找的数据
        :return: Node
        """
        node: Node = self.__head
        while node is not None and node.data != value:
            node = node.next
        return node

    def find_by_index(self, index: int) -> Node:
        """按照索引寻找对应的节点
        :param index:
        :return: Node
        """
        node = self.__head
        pos: int = 0
        while node is not None and pos != index:
            node = node.next
            pos += 1
        return node

    def insert_to_head(self, value: object):
        """头节点前插入节点
        :param value:
        :return:
        """
        node = Node(value)
        node.next = self.__head
        self.__head =node

    def insert_after(self, node: Node, value: object):
        """在指定的node节点之后插入一个值为value的新节点
        :param node:
        :param value:
        :return:
        """
        if node is None:
            return
        new_node = Node(value)
        new_node.next = node.next
        node.next = new_node

    def insert_before(self, node: Node, value: object):
        """在指定的node节点之前插入一个值为value的新节点
        :param node:
        :param value:
        :return:
        """
        if node is None or self.__head is None:
            return

        if node == self.__head:
            self.insert_to_head(value)

        new_node = Node(value)
        pre = self.__head
        not_found= False
        while pre != node:
            if pre is None:
                not_found = True
                break
            else:
                pre = pre.next

        if not_found is False:
            pre.next = new_node
            new_node.next = node

    def delete_by_node(self, node: Node):
        if self.__head is None:
            return
        if node == self.__head:
            self.__head = node.next
            return

        pre: Node = self.__head
        not_found = False
        while pre.next is not node:
            if pre.next is None:
                not_found = True
                break
            else:
                pre = pre.next
        if not_found is False:
            pre.next = node.next



