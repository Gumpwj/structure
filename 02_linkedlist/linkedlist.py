
# 链表节点
class Node(object):
    def __init__(self, value=None, next=None):
        self.value, self.next = value, next


# 单链表
class LinkedList(object):
    """单向链表
    """
    def __init__(self, maxsize=None):
        self.maxsize = maxsize
        self.root = Node()
        self.length = 0
        self.tailnode = None

    def __len__(self):
        return self.length

    """ 尾节点加一个节点 """
    def append(self, value):
        if self.maxsize is not None and self.length > self.maxsize:
            raise Exception('full')
        node = Node(value)
        tailnode = self.tailnode
        if tailnode is None:
            self.root.next = node
        else:
            tailnode.next = node
        self.tailnode = node
        self.length += 1

    def appendleft(self, value):
        headnode = self.root.next
        node = Node(value)
        self.root.next = node
        node.next = headnode
        self.length += 1


    """ 遍历从head节点->tail节点 """
    def iter_node(self):
        current = self.root.next
        while current is not self.tailnode:
            yield current
            current = current.next

    def __iter__(self):
        for node in self.iter_node():
            yield node.value

    """ 删除当前节点把前一个节点指向当前节点的下一个节点 """
    def remove(self, value):  # O(n)
        previous = self.root
        current = self.root.next
        for current in self.iter_node():
            if current.value == value:
                previous.next = current.next
                if current is self.tailnode:
                    self.tailnode = previous
                del current
                self.length -= 1
                return 1
            else:
                previous = current
        return -1

    def find(self, value):   # O(n)
        index = 0
        for node in self.iter_node():
            if node.value == value:
                return index
            index += 1
        return -1
    
    def popleft(self):
        if self.root.next is None:
            raise Exception('pop from empty linkedlist')
        headnode = self.root.next
        self.root.next = headnode.next
        self.length -= 1
        value = headnode.value
        del headnode
        return value

    def clear(self):
        for node in self.iter_node():
            del node
        self.root.next = None
        self.length = 0

    


linkedlist_ = LinkedList(50)

# 测试单链表
def test_linkedlist():
    ll = LinkedList()
    
    ll.append(1)
    ll.append(2)
    ll.append(3)
    ll.append(4)

    for i in ll:
        print(i)

    assert ll.find(1) == 0
    
    assert ll.remove(2) == 1
    assert len(ll) == 3
    
    assert ll.remove(10) == -1

    assert ll.popleft() == 1
    assert len(ll) ==2

    ll.clear()
    assert len(ll) == 0

    ll.appendleft(4)
    ll.appendleft(5)
    assert len(ll) == 2
    assert ll.find(4) == 1
    assert ll.find(5) == 0

    from collections.abc import Iterable
    assert isinstance(ll, Iterable)

    # assert False
    # assert 0

    
        



    